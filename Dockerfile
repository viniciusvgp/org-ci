FROM ubuntu:jammy

ENV DEBIAN_FRONTEND noninteractive

RUN apt update \
    && apt install -y git python3 curl autoconf make file emacs texlive-latex-base texlive-fonts-recommended texlive-latex-recommended texlive-latex-extra latexmk ispell texlive-lang-portuguese texlive-publishers texlive-pictures texlive-luatex imagemagick-6.q16 fonts-tlwg-purisa-otf \
    && apt autoremove -y \
    && apt clean -y \
    && rm -rf /var/lib/apt/lists/*

RUN apt update && apt install -y texlive-luatex

RUN useradd -s /bin/bash --create-home user
USER user

ENTRYPOINT /bin/bash
WORKDIR /home/user

RUN mkdir misc && cd misc && git clone https://code.orgmode.org/bzg/org-mode.git && cd org-mode && git checkout release_9.3.6 && make autoloads && cd ../..
RUN git clone https://github.com/viniciusvgp/emacs-init.git ~/.emacs.d && cd ~/.emacs.d && git checkout 5c769396787b372fe61091505df53bfdb33917fb && emacs -batch init.org  --funcall org-babel-tangle && emacs --script init.el && cd ..
